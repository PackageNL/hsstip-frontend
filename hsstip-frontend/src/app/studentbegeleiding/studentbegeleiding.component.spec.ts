import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { StudentbegeleidingComponent } from './studentbegeleiding.component';

describe('StudentbegeleidingComponent', () => {
  let component: StudentbegeleidingComponent;
  let fixture: ComponentFixture<StudentbegeleidingComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ StudentbegeleidingComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(StudentbegeleidingComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
