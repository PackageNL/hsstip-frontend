import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-docent',
  templateUrl: './docent.component.html',
  styleUrls: ['./docent.component.css']
})
export class DocentComponent implements OnInit {

  constructor() { }

  isValid: boolean= true;

  ngOnInit() {
  }

  openContent(evt, inside) {
    let i, content, tablinks;
    content = document.getElementsByClassName("tab-content");
    for (i = 0; i < content.length; i++) {
      content[i].style.display = "none";
    }
    tablinks = document.getElementsByClassName("tablink");
    for (i = 0; i < content.length; i++) {
      tablinks[i].className = tablinks[i].className.replace("active", "");
    }
    document.getElementById(inside).style.display = "block";
    // evt.currentTarget.className += " selected";
    document.getElementById(evt).className += " active";
  }

  changeValue(valid: boolean) {
    this.isValid = valid;
  }
}
