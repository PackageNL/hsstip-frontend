import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EindbeoordelingComponent } from './eindbeoordeling.component';

describe('EindbeoordelingComponent', () => {
  let component: EindbeoordelingComponent;
  let fixture: ComponentFixture<EindbeoordelingComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EindbeoordelingComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EindbeoordelingComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
