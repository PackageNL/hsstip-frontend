import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { StageVoorstellenComponent } from './stage-voorstellen.component';

describe('StageVoorstellenComponent', () => {
  let component: StageVoorstellenComponent;
  let fixture: ComponentFixture<StageVoorstellenComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ StageVoorstellenComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(StageVoorstellenComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
