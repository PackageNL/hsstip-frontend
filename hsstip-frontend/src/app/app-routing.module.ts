import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { DashboardComponent } from './dashboard/dashboard.component';
import { DocentComponent } from './docent/docent.component';
import { BedrijvenComponent } from './bedrijven/bedrijven.component';
import { StagesComponent } from './stages/stages.component';
import { StudentenComponent } from './studenten/studenten.component';
import { LoginComponent } from "./login/login.component";
import { AuthGuard } from "./auth.guard";
import { StudentOverzichtComponent } from "./studentOverzicht/studentOverzicht.component";
import { StageVoorstellenComponent } from "./stage-voorstellen/stage-voorstellen.component";
import { EindbeoordelingComponent } from "./eindbeoordeling/eindbeoordeling.component";
import { StudentbegeleidingComponent } from "./studentbegeleiding/studentbegeleiding.component";

const AppRoutes: Routes = [
  { path: '', component: LoginComponent},
  { path: 'login', component: LoginComponent},
  { path: 'dashboard',
    // canActivate:[AuthGuard], TODO: set active
    component: DashboardComponent},
  { path: 'studentbegeleiding', component: StudentbegeleidingComponent},
  { path: 'stagevoorstellen',component: StageVoorstellenComponent},
  { path: 'eindbeoordeling', component: EindbeoordelingComponent},
  { path: 'docenten', component: DocentComponent},
  { path: 'studenten', component: StudentenComponent},
  { path: 'studentoverzicht', component: StudentOverzichtComponent},
  { path: 'stages', component: StagesComponent},
  { path: 'bedrijven', component: BedrijvenComponent}


];

@NgModule({
  imports: [
    RouterModule.forRoot(
      AppRoutes
    )
  ],
  exports: [
    RouterModule
  ]

})

export class AppRoutingModule {}
