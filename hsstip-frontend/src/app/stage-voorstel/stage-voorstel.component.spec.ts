import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { StageVoorstelComponent } from './stage-voorstel.component';

describe('StageVoorstelComponent', () => {
  let component: StageVoorstelComponent;
  let fixture: ComponentFixture<StageVoorstelComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ StageVoorstelComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(StageVoorstelComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
