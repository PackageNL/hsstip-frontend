import { Component, OnInit } from '@angular/core';
import {Router} from "@angular/router";

@Component({
  selector: 'app-studenten',
  templateUrl: './studenten.component.html',
  styleUrls: ['./studenten.component.css']
})
export class StudentenComponent implements OnInit {

  constructor(private router: Router) {
  }

  ngOnInit() {
  }

  openContent(evt, inside) {
    let i, content, tablinks;
    content = document.getElementsByClassName("tab-content");
    for (i = 0; i < content.length; i++) {
      content[i].style.display = "none";
    }
    tablinks = document.getElementsByClassName("tablink");
    for (i = 0; i < content.length; i++) {
      tablinks[i].className = tablinks[i].className.replace("active", "");
    }
    document.getElementById(inside).style.display = "block";
    // evt.currentTarget.className += " selected";
    document.getElementById(evt).className += " active";
  }

}
